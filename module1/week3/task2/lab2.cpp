#include <iostream>
#include <regex>

using namespace std;

string validIPAddress(string IP) {
    regex ipv6("((([0-9a-fA-F]){1,4})\:){7}([0-9a-fA-F]){1,4}");
    if(regex_match(IP, ipv6)) {
        return "Это IPv6";
    } 
    else {
        return "Это не IPv6";
    }
}

int main(){
    string IP; // "2001:82a8:82a3:0000:0000:8B2E:0270:7224";
    cout << "Введите ваш IPv6: ";
    getline(cin, IP);
    string ans = validIPAddress(IP);
    cout << ans << endl;
    return 0;
}
