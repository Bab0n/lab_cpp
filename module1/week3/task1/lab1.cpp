#include <iostream>
#include <string>

using namespace std;

void find(string str, string substr) {
    int occurrences = 0;
    size_t start = 0;
    while ((start = str.find(substr, start))!= string::npos) {
        occurrences++;
        start = str.find(substr, start + 1);
    }
    cout << occurrences;
}

int main() {
    string str, substr;
    cout << "Что ищем: " << endl; 
    getline(cin, substr);
    cout << "Где ищем: " << endl;
    getline(cin, str, ' '); 
    cout << "количество вхождений одной строки в N других: ";
    find(str, substr);
}
