#include <iostream>
#include <iomanip>
#include <string>
#include <cmath>

using namespace std;

void Entrophy(string str)
{
    double probability , entrophy = 0;
    string alphabet{"ABCDEFGHIJKMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz "};
    int freq[512] = {0};

    for (auto c : str) {
        freq[c]++;
    }

    int count = 0;
    for (auto element : alphabet) {
        count += freq[element];
    }

    for (auto element : alphabet) {
        probability  = double(freq[element]) / count; //вероятность появления символа

        if (probability  != 0)
            entrophy += -(probability  * log2(probability )); //энтропия Шеннона

    }
    cout << setprecision(3) << entrophy << endl;
}

int main() {
	string str;
	cout << "Ввод: " << endl;
	getline(cin, str);
	cout << endl << "Вывод: " << endl;
	Entrophy(str);
	return 0;
}
