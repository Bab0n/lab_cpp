#include <iostream>
#include <limits>
using namespace std;
int main()
{
    int max = numeric_limits<int>::min(), k = 0, n;
    cout << "Введите размерность массива: ";
    cin >> n;
    if (n<=0){
        cout << "Размерность массива меньше или равно 0"<<endl; 
        return 1;
    }
    else {
        cout << "Размерность массива больше 0"<<endl;
    }
    int *arr = new int[n]; //выделение памяти для массива из n элементов
    cout << "Массив: ";
    for (int i = 0; i < n; ++i) { //цикл для создания рандомных элементов массива n размерностью 
        arr[i] = rand() % (2*n+1) - n;
        cout << arr[i] << " ";
    }
    for (int i = 0; i < n; i++) { //цикл для подсчёта чисел, равных максимальному
        if (arr[i] > max){
            max = arr[i];
            k = 1;
    }
        else {
            if (max == arr[i]) {
                k++;
            }
        }
    }
    cout << "\n" << "Максимальный элемент массива: "<< max << endl;
    cout << "Количество чисел, равных максимальному: " << k;
    delete[] arr; //очистка памяти, выделенной для массива
    return 0;
}
