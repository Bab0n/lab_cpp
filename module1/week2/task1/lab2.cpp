#include <iostream>
#include <string>
#include <ctime>

using namespace std;

int main()
{
    setlocale(LC_ALL, "Russian");
    int month;
    time_t t = time(nullptr);
    tm *const pTInfo = localtime(&t);
    cout << "Сейчас " << 1900 + pTInfo->tm_year << " год" << '\n'; // выводит текущий год
    cout << "Введите месяц от 1 до 12: ";
    cin >> month;
    switch (month){
        case 1: case 3: case 5: case 7: case 8: case 10: case 12:
            cout << "В данном месяце 31 день"<< endl;
            break;
        case 4: case 6: case 9: case 11:
            cout << "В данном месяце 30 дней" << endl;
            break;
        case 2:
            if ((1900 + pTInfo->tm_year) % 4 == 0){ // определяем является ли год високосным или нет
                cout << "Текущий год високосный, а значит в месяце 29 дней" << endl;
            }
            else {
                cout << "Текущий год не високосный, а значит в месяце 28 дней" << endl;
            }
            break;
        default:
            cout << "Вы ввели не целое число или число не с диапозона! Попробуйте снова..." << endl;;
            break;
    }
    return 0;
}

